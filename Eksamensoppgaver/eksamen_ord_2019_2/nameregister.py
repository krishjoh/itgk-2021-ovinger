# module nameregister 

def days_in_month(month): 
    return [31,28,31,30,31,30,31,31,30,31,30,31][month-1] 

def register_name(people, fnr, name): 
    people[fnr] = name 

def find_name(people, fnr): 
    return people.get(fnr, '') 